///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo Jayson Iwanaka <@todo jiwanaka@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo 05_05_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"
#define NAME_LENGTH 6

#include "cat.hpp"

using namespace std;


Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

/*bool CatEmpire::operator>(const Cat& rightSide) {
	// this is the leftSide of the operator, so compare:
	// leftSide > rightSide

	if( this > &rightSide )
		return true;
	return false;
}*/


void CatEmpire::addCat( Cat* newCat ){
	newCat->left = nullptr;
	newCat->right = nullptr;

	if(topCat == nullptr){
		//cout<<"test3"<<endl;
		topCat = newCat;
		return;
	}

	addCat(topCat, newCat);
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
	//cout<<"test"<<endl;
if(atCat->name > newCat->name){
	if (atCat->left == nullptr){
		atCat->left = newCat;
	//	cout<<"test1"<<endl;
	} else {
		//cout<<"test2"<<endl;
		addCat(atCat->left, newCat);
	}
}	
	if (atCat->name < newCat->name){
		if (atCat->right == nullptr){
			atCat->right = newCat;
		}
		else
		{
			addCat(atCat->right, newCat);
		}
	}


}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, deep );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth) const{
	//cout << string( NAME_LENGTH * (depth-1), ' ') << atCat->name;
	depth++;
	//cout<<"\\"<<endl;

	if(atCat->left == nullptr && atCat->right == nullptr){
		cout << string( NAME_LENGTH * (depth-1), ' ') << atCat->name;
		cout<<endl;
	} 
	
	if (atCat->left && atCat->right){
		dfsInorderReverse(atCat->right, depth);
		cout << string( NAME_LENGTH * (depth-1), ' ') << atCat->name;
		cout<<"<"<<endl;
		dfsInorderReverse(atCat->left, depth);
	} else if (atCat->left == nullptr && atCat->right !=nullptr ){
		dfsInorderReverse(atCat->right, depth);
		cout << string( NAME_LENGTH * (depth-1), ' ') << atCat->name;
		cout<<"/"<<endl;
	//	dfsInorderReverse(atCat->left, depth);
	} else if (atCat->left != nullptr && atCat->right == nullptr) {
		
		cout << string( NAME_LENGTH * (depth-1), ' ') << atCat->name;
		cout<<"\\"<<endl;
		dfsInorderReverse(atCat->left, depth);
		
	}

	return;
}


void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat ) const{
	if(atCat == nullptr){
		return;
	} else {
		//cout << atCat->name << endl;
		dfsInorder(atCat->left);
		cout << atCat->name << endl;
		dfsInorder(atCat->right);
		
	}
}



void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const{
	if(atCat == nullptr){
		return;
	} else if(atCat->left && atCat->right){
		cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
		dfsPreorder(atCat->left);
		dfsPreorder(atCat->right);
	} else if(atCat->left){
		cout << atCat->name << " begat " << atCat->left->name << endl;
		dfsPreorder(atCat->left);
		dfsPreorder(atCat->right);
	} else if(atCat->right){
		cout << atCat->name << " begat " << atCat->right->name << endl;
		dfsPreorder(atCat->left);
		dfsPreorder(atCat->right);
	}
}

void CatEmpire::catGenerations() const 
{ 
	if(topCat == nullptr) return;

	std::queue<Cat*> q;
	int num = 1;
  
    q.emplace(topCat); 
  
    while (q.empty() == false) 
    { 
        long unsigned int nodeCount = q.size(); 

		getEnglishSuffix(num);

		cout<<" Generation"<<endl;
  
        while (nodeCount > 0)
        { 
            Cat *cat = q.front(); 
            cout <<"  "<< cat->name <<" "; 
            q.pop(); 
            if (cat->left != NULL) 
                q.push(cat->left); 
            if (cat->right != NULL) 
                q.push(cat->right); 
            nodeCount--; 
        } 
        cout << endl; 
		num++;
    } 
}

/*void CatEmpire::catGenerations() const{
	std::queue<Cat*> catQueue;
	int duck = 1;
	int number = generation;

//	cout << " " << duck << getEnglishSuffix(duck) << " Generation" << endl;

	catQueue.emplace(topCat);
	
	for(int gen=1; !catQueue.empty(); gen++){

		Cat* cat = catQueue.front();
		catQueue.pop();
		cout<< cat->name << endl;

		if(cat->left != nullptr){
			catQueue.push(cat->left);
			cout << "   " << cat->left->name;
			
			}
		if(cat->right != nullptr){
			catQueue.push(cat->right);
			cout << "   " << cat->right->name;

		}			
			number = gen;
	}
	cout<<number<<endl;
}
*/

void CatEmpire::getEnglishSuffix(int n) const{
   if(n%100 == 11 || n%100 == 12 || n%100 == 13)
    {
      cout << n << "th";
    }
  else
    {
      if(n%10 == 1)
    {
      cout << n << "st";
    }
      else
    {
      if(n%10 == 2)
        {
          cout << n << "nd";
        }
      else
        {
          if(n%10 == 3)
        {
          cout << n << "rd";
        }
          else
        {
          if(n%10 == 4 || n%10 == 5 || n%10 == 6 || n%10 == 7 || n%10 == 8 || n%10 == 9 || n%10 == 0)
              {
            cout << n << "th";
              }
        }
        }
    }
    }
}

/*	if(n <= 0){
		cout<<"I can't let you do that, Star Fox..."<<endl;
		return;
	}
	else if(n == 1){
		cout<<"st";
		return;
	}
	else if(n == 2){
		cout<<"nd";
		return;
	}
	else if(n == 3){
		cout<<"rd";
		return;
	}
	else if(n >= 4){
		cout<<"th";
		return;
	}
	return; 
*/